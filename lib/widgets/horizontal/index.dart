import 'package:extended_image/extended_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fstore/generated/i18n.dart';
// import 'package:fstore/generated/i18n.dart';

// import '../../common/constants.dart';
// import 'banner_animate_items.dart';
// import 'banner_group_items.dart';
// import 'banner_slider_items.dart';
// import 'blog_list_items.dart';
// import 'category_icon_items.dart';
// import 'category_image_items.dart';
// import 'instagram_items.dart';
// import 'product_list_items.dart';

class HorizontalList extends StatefulWidget {
  final configs;

  HorizontalList({this.configs});

  @override
  _HorizontalListState createState() => _HorizontalListState();
}

class _HorizontalListState extends State<HorizontalList>
    with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;

  /// convert the JSON to list of horizontal widgets
  //Widget jsonWidget(config) {
  // switch (config["layout"]) {
  //   case "logo":
  //     return Container(
  //margin: EdgeInsets.only(left: 15, right:15),
  // child: Stack(
  //   children: <Widget>[
  // Positioned(
  //   top: 55,
  //   left: 10,
  //   child: IconButton(
  //     icon: Icon(
  //       Icons.blur_on,
  //       color: Theme.of(context).accentColor.withOpacity(0.6),
  //       size: 22,
  //     ),
  //     onPressed: () => Scaffold.of(context).openDrawer(),
  //   ),
  // ),
  //       Center(
  //         child: Padding(
  //           child: Image.asset('assets/images/top.jpg'),
  //           padding: EdgeInsets.only(
  //               bottom: 5.0, top: 3.0, left: 15.0, right: 15.0),
  //         ),
  //       ),
  //     ],
  //   ),
  // );
  //case "bannerAnimated":
  //return BannerAnimated(config: config);
  //   case "bannerImage":
  //     return config['isSlider'] == true
  //         ? BannerSliderItems(config: config)
  //         : BannerGroupItems(config: config);
  //   case "iconCategory":
  //     return CategoryIcons(config: config);
  //   case "imageCategory":
  //     return CategoryImages(config: config);
  //     break;
  //   case "instagram":
  //     return InstagramItems(config: config);
  //   case "blog":
  //     var blogUrl = widget.configs["server"]["blog"];
  //     return BlogListItems(config: config, url: blogUrl);
  //   default:
  //     return ProductListItems(config: config);
  // }
  //}

  @override
  Widget build(BuildContext context) {
    super.build(context);

    if (widget.configs == null) return Container();

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Center(
            child: Padding(
              child: Image.asset('assets/images/top.jpg'),
              padding: EdgeInsets.only(
                  bottom: 5.0, top: 3.0, left: 15.0, right: 15.0),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 2.4,
                margin: EdgeInsets.only(bottom: 8.0),
                height: 35.0,
                color: Colors.brown[50],
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          Icons.check_box_outline_blank,
                          color: Colors.brown[200],
                        ),
                        onPressed: () {}),
                    Text(
                      'عبايات سادة',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 2.4,
                color: Colors.brown[50],
                height: 35.0,
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          Icons.close,
                          color: Colors.brown[200],
                        ),
                        onPressed: () {}),
                    Text(
                      'عبايات عملية',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 2.4,
                margin: EdgeInsets.only(bottom: 8.0),
                height: 35.0,
                color: Colors.brown[50],
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          FontAwesomeIcons.diceD20,
                          color: Colors.brown[200],
                        ),
                        onPressed: () {}),
                    Text(
                      'عبايات مناسبات',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 2.4,
                color: Colors.brown[50],
                height: 35.0,
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          FontAwesomeIcons.chessQueen,
                          color: Colors.brown[200],
                        ),
                        onPressed: () {}),
                    Text(
                      'تصاميم مميزة',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ],
          ),
//here
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 2.4,
                margin: EdgeInsets.only(bottom: 8.0),
                height: 35.0,
                color: Colors.brown[50],
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          FontAwesomeIcons.shirtsinbulk,
                          color: Colors.brown[200],
                        ),
                        onPressed: () {}),
                    Text(
                      'عبايات سفر',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 2.4,
                color: Colors.brown[50],
                height: 35.0,
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          FontAwesomeIcons.clock,
                          color: Colors.brown[200],
                        ),
                        onPressed: () {}),
                    Text(
                      'عبايات ملونة',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 2.4,
                margin: EdgeInsets.only(bottom: 8.0),
                height: 35.0,
                color: Colors.brown[50],
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          FontAwesomeIcons.dizzy,
                          color: Colors.brown[200],
                        ),
                        onPressed: () {}),
                    Text(
                      'عبايات الربيع',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 2.4,
                color: Colors.brown[50],
                height: 35.0,
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          FontAwesomeIcons.shoppingBag,
                          color: Colors.brown[200],
                        ),
                        onPressed: () {}),
                    Text(
                      ' عبايات رسمية',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Container(
                width: MediaQuery.of(context).size.width / 2.4,
                margin: EdgeInsets.only(bottom: 8.0),
                height: 35.0,
                color: Colors.brown[50],
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          FontAwesomeIcons.angleDown,
                          color: Colors.brown[200],
                        ),
                        onPressed: () {}),
                    Text(
                      'قسم الخصومات',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 2.4,
                color: Colors.brown[50],
                height: 35.0,
                child: Row(
                  children: [
                    IconButton(
                        icon: Icon(
                          FontAwesomeIcons.chess,
                          color: Colors.brown[200],
                        ),
                        onPressed: () {}),
                    Text(
                      ' تشكيلة العيد',
                      style: TextStyle(
                          fontSize: 16.0, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
            ],
          ),

          Container(
            width: 325.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                  Stack(
                    children: [
                      ExtendedImage.asset(
                        'assets/images/fogg-delivery-1.png',
                        height: 105.0,
                        width: 100.0,
                        fit: BoxFit.fill,
                       // color: Colors.brown[50],
                      ),
                      Positioned(
                        right: 35.0,
                        top: 15.0,
                        child: Row(
                          children: [
                            //Text('قسم \n'),
                            Text.rich(
                              TextSpan(
                               text: 'قسم\n',
                                children: [
                                  TextSpan(
                                      text: 'النقابات',
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                 Stack(
                    children: [
                      ExtendedImage.asset(
                        'assets/images/fogg-uploading-1.png',
                        height: 105.0,
                        width: 100.0,
                        fit: BoxFit.fill,
                       // color: Colors.brown[50],
                      ),
                      Positioned(
                        right: 35.0,
                        top: 15.0,
                        child: Row(
                          children: [
                            //Text('قسم \n'),
                            Text.rich(
                              TextSpan(
                               text: 'قسم\n',
                                children: [
                                  TextSpan(
                                      text: 'الطرح',
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                   Stack(
                    children: [
                      ExtendedImage.asset(
                        'assets/images/fogg-order-completed.png',
                        height: 105.0,
                        width: 100.0,
                        fit: BoxFit.cover,
                       // color: Colors.brown[50],
                      ),
                      Positioned(
                        right: 35.0,
                        top: 15.0,
                        child: Row(
                          children: [
                            //Text('قسم \n'),
                            Text.rich(
                              TextSpan(
                               text: 'قسم\n',
                                children: [
                                  TextSpan(
                                      text: 'الاكل',
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold)),
                                ],
                              ),
                              textAlign: TextAlign.center,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
              ],
            ),
          ),
          //for (var config in widget.configs["HorizonLayout"]) jsonWidget(config)
        ],
      ),
    );
  }
}
